# SSL scan

## Runner executor

The jobs run in containers, so this project won't work with the shell runner. Use and executor that supports `image:` such as docker, docker machine, or kubernetes.

## Pipeline overview
This project creates a CI pipeline with a debug job, plus two manual jobs.

One manual job scans the GitLab instance, the other job scans the GitLab registry.

This may be useful insight on its own.

It also acts as a PoC for performing TLS testing in CI - perhaps as part of set of DAST tools.
If you're deploying to a site and have the URL already inside your pipeline, substitute that it.
Additional work would be required to surface the test results into the GitLab UI.

## Potentially useful options

- could be useful, but only for output files.

```
--severity <severity> For CSV and both JSON outputs this will only add findings
to the output file if a severity is equal or higher than the severity value specified.
Allowed are <LOW|MEDIUM|HIGH|CRITICAL>. WARN is another level which translates to a
client-side scanning error or problem. Thus you will always see them in a file if they occur.
```

By default lots of tests are run.  Specific tests can be enabled, like:

- `-p, --protocols checks TLS/SSL protocols SSLv2, SSLv3, TLS 1.0 through TLS 1.3 and for HTTP`
- `-h, --header, --headers `  (which includes the HSTS headers)

See [the online man page](-h, --header, --headers)

## SSL labs

The output ends with comparable SSL labs conclusion:

```
 Rating (experimental) 
 Rating specs (not complete)  SSL Labs's 'SSL Server Rating Guide' (version 2009q from 2020-01-30)
 Specification documentation  https://github.com/ssllabs/research/wiki/SSL-Server-Rating-Guide
 Protocol Support (weighted)  100 (30)
 Key Exchange     (weighted)  90 (27)
 Cipher Strength  (weighted)  90 (36)
 Final Score                  93
 Overall Grade                A+
 Done 2022-03-31 09:34:55 [ 208s] -->> 172.65.251.78:443 (gitlab.com) <<--
```
